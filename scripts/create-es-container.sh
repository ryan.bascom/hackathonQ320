#!/bin/bash

RID=$(docker ps -qa -f name=hackathonQ320-es)
if [ ! "${RID}" ]; then
  echo "Container doesn't exist"
  docker run --name hackathonQ320-es -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.9.2
else
  echo "Container already exists, ensuring running"
  docker start hackathonQ320-es
fi