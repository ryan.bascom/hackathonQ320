#!/bin/bash

if docker start hackathonQ320-es; then
  echo "Elasticsearch container started"
else
  echo "Unable to start elasticserach container try running make install"
  exit 1
fi